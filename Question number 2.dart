/*
Create an array to store all the winning apps of the 
MTN Business App of the Year Awards since 2012; 
  a) Sort and print the apps by name;  
  b) Print the winning app of 2017 and the winning app of 2018.; 
  c) the Print total number of apps from the array.
*/

void main() {
  // Array of MTN Business App of the Year Awards since 2012
  List<String> winningApps = [
    'FNB Banking App', // 2012 Winning App
    'Bookly', // 2013 Winning App
    'Live Inspect', // 2014 Winning App
    'CPUT Mobile', // 2015 Winning App
    'Domestly', // 2016 Winning App
    'Orderin', // 2017 Winning App
    'Cowa-Bunga', // 2018 Winning App
    'Digger', // 2019 Winning App
    'Checkers Sixty 60', // 2020 Winning App
    'Ambani Afrika' // 2021 Winning App
  ];

  // A - Sort apps by name
  winningApps.sort();

  // A - Print apps by name
  print(winningApps.join('\n'));

  // Print 2017 & 2018 winning apps
  print("\n2017 & 2018 winning apps:");
  print(winningApps[9]);
  print(winningApps[4]);

  // Print total number of apps
  print("\nArray Length: ${winningApps.length}");
}
