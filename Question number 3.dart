/*
Create a class and 
a) then use an object to print the name of the app, sector/category, developer, and the year it won MTN Business App of the Year Awards. 
b) Create a function inside the class, transform the app name to all capital letters and then print the output.
*/

class App {
  String? name;
  String? sector;
  String? developer;
  int? year;

  App(String name, String sector, String developer, int year) {
    this.name = name;
    this.developer = developer;
    this.sector = sector;
    this.year = year;
  }

  void capitalizeName() {
    this.name = this.name?.toUpperCase();
  }

  void printDetails() {
    print(this.name);
    print(this.developer);
    print(this.year);
    print(this.sector);
  }
}

void main() {
  App app1 = new App("Assignment", "Education", "Katlego", 2022);

  app1.printDetails();
  print("\n\n");
  app1.capitalizeName();
  app1.printDetails();
}
